
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class Main {

    private final static String CHARSET = "UTF-8";
    private final static String ENCRYPT_MODE = "AES";

    private final static String password = "thisismypassword";
    private final static String msg = "This is a text message.";
    private final static String jsonString = "{\\r\\n  \\\"code\\\" : \\\"ATB\\\",\\r\\n  \\\"description\\\" : \\\"Air Turn Back\\\"," +
            "\\r\\n  \\\"updateDate\\\" : {\\r\\n    \\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    " +
            "\\\"epochTime\\\" : 1516110809341\\r\\n  }\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"BST\\\",\\r\\n  " +
            "\\\"description\\\" : \\\"Bird Strike\\\",\\r\\n  \\\"updateDate\\\" : {\\r\\n    \\\"label\\\" : " +
            "\\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    \\\"epochTime\\\" : 1516110809341\\r\\n  }\\r\\n}, " +
            "{\\r\\n  \\\"code\\\" : \\\"DVT\\\",\\r\\n  \\\"description\\\" : \\\"Divertion\\\",\\r\\n  " +
            "\\\"updateDate\\\" : {\\r\\n    \\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    " +
            "\\\"epochTime\\\" : 1516110809341\\r\\n  }\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"EAT\\\",\\r\\n  " +
            "\\\"description\\\" : \\\"Emergency activated by technical issues\\\",\\r\\n  \\\"updateDate\\\" : " +
            "{\\r\\n    \\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    \\\"epochTime\\\" : " +
            "1516110809341\\r\\n  }\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"GOC\\\",\\r\\n  \\\"description\\\" : " +
            "\\\"Ground Occurrence\\\",\\r\\n  \\\"updateDate\\\" : {\\r\\n    \\\"label\\\" : " +
            "\\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    \\\"epochTime\\\" : 1516110809341\\r\\n  }\\r\\n}, " +
            "{\\r\\n  \\\"code\\\" : \\\"GTB\\\",\\r\\n  \\\"description\\\" : \\\"Ground Turn Back\\\",\\r\\n  " +
            "\\\"updateDate\\\" : {\\r\\n    \\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    " +
            "\\\"epochTime\\\" : 1516110809341\\r\\n  }\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"IFSD\\\",\\r\\n  " +
            "\\\"description\\\" : \\\"In Flight Shut Down\\\",\\r\\n  \\\"updateDate\\\" : {\\r\\n    " +
            "\\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    \\\"epochTime\\\" : 1516110809341\\r\\n  " +
            "}\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"OTH\\\",\\r\\n  \\\"description\\\" : \\\"Others\\\",\\r\\n  " +
            "\\\"updateDate\\\" : {\\r\\n    \\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    " +
            "\\\"epochTime\\\" : 1516110809341\\r\\n  }\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"RTO\\\",\\r\\n  " +
            "\\\"description\\\" : \\\"Rejected Take Off\\\",\\r\\n  \\\"updateDate\\\" : {\\r\\n    " +
            "\\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    \\\"epochTime\\\" : 1516110809341\\r\\n  " +
            "}\\r\\n}, {\\r\\n  \\\"code\\\" : \\\"SML\\\",\\r\\n  \\\"description\\\" : \\\"Smell of " +
            "smoke\\/burned\\/Hazmat contamination in cabin\\\",\\r\\n  \\\"updateDate\\\" : {\\r\\n    " +
            "\\\"label\\\" : \\\"2018-01-16T13:53:29.341Z\\\",\\r\\n    \\\"epochTime\\\" : 1516110809341\\r\\n  " +
            "}\\r\\n}";

    public static void main (String[] args) {

        //String mySecretKey = "";
        //String mySecretKey_staging = "5OrZbF/ov+y8JP+Y/O3PAvcCiNmZ+gKFaPVkyCa0wLI=";
        String mySecretKey = "uJjzMyi9ai5MBBjbyMzZxC3GQiu+lNPrhXs5mMHQNF0=";

        /*
        try {
            mySecretKey = Base64.getEncoder().encodeToString(generateKEY().getEncoded());
            System.out.println("My key: " + mySecretKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        */


        try {
            String encryptPassword = encrypt(password, mySecretKey);
            String encryptMsg = encrypt(msg, mySecretKey);
            String encryptJSON = encrypt(jsonString, mySecretKey);

            System.out.println("password encrypt to>>>>>>> " + encryptPassword);
            System.out.println("password decrypt to>>>>>>> " + decrypt(encryptPassword, mySecretKey));

            System.out.println("msg encrypt to>>>>>>> " + encryptMsg);
            System.out.println("msg decrypt to>>>>>>> " + decrypt(encryptMsg, mySecretKey));

            System.out.println("jsonString encrypt to>>>>>>> " + encryptJSON);
            System.out.println("jsonString encrypt to>>>>>>> " + decrypt(encryptJSON, mySecretKey));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param strClearText
     * @return
     * @throws Exception
     */
    private static String encrypt (String strClearText, String key) throws Exception {
        String strData;
        SecretKey secretKey = stringToKey(key);

        try {
            SecretKeySpec skeyspec = new SecretKeySpec(secretKey.getEncoded(), ENCRYPT_MODE);
            Cipher cipher = Cipher.getInstance(ENCRYPT_MODE);
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted = cipher.doFinal(strClearText.getBytes(CHARSET));
            strData = Base64.getEncoder().encodeToString(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }

    /**
     *
     * @param strEncrypted
     * @return
     * @throws Exception
     */
    private static String decrypt (String strEncrypted, String key) throws Exception {
        String strData;
        SecretKey secretKey = stringToKey(key);
        byte[] bytesEncrypted = Base64.getDecoder().decode(strEncrypted);

        try {
            SecretKeySpec skeyspec = new SecretKeySpec(secretKey.getEncoded(), ENCRYPT_MODE);
            Cipher cipher = Cipher.getInstance(ENCRYPT_MODE);
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);
            byte[] decrypted = cipher.doFinal(bytesEncrypted);
            strData = new String(decrypted);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }

    private static SecretKey generateKEY() throws NoSuchAlgorithmException {
        SecureRandom r = new SecureRandom();

        byte[] newSeed = r.generateSeed(32);
        r.setSeed(newSeed);

        KeyGenerator keyGen = KeyGenerator.getInstance("AES");

        SecureRandom sRandom = SecureRandom.getInstanceStrong();

        keyGen.init(256, sRandom);

        return keyGen.generateKey();

    }

    private static SecretKey stringToKey(String secretKey) {
        byte[] decodedSecretKey = Base64.getDecoder().decode(secretKey);
        return new SecretKeySpec(decodedSecretKey, 0, decodedSecretKey.length, "AES");
    }
}
